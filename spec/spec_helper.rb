ENV['APP_ENV'] ||= 'test'
$:.unshift File.dirname(__FILE__) + "/../lib"

require 'trainline'

require 'minitest/autorun'
