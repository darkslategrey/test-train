require 'spec_helper'

describe Trainline::Utils do

  subject { Trainline::Utils }

  describe '#gunzip_to_hash' do

    let(:json_response) {
      "#{File.dirname(__FILE__)}/../fixtures/response.json"
    }

    let(:zipped) {
      out      = StringIO.new
      zipped   = Zlib::GzipWriter.new(out)
      zipped.write(IO.read(json_response)); zipped.close
      out.string
    }

    it 'should_return_a_hash' do
      unzipped = subject.gunzip_to_hash(zipped)
      unzipped[:folders].wont_be_nil
    end
  end

  describe '#to_date' do
    let(:expected) { '1970-01-01T01:00:00+01:00' }
    it 'should_do_conversion' do
      subject.to_date('01/01/70 01:00').must_equal expected
    end
  end

  describe '#to_time' do
    it 'should_do_conversion' do
      subject.to_time('1970-01-01T01:00:00+01:00').must_equal '01:00'
    end
  end

  describe '#format_price' do
    it 'should_do_formating' do
      subject.format_price('1234').must_equal '12,34 Euros'
    end
  end

  describe '#elapsed_time' do
    let(:dep) { DateTime.now }
    let(:arr) { dep + 2.hours + 13.minutes }

    it 'should_show_elapsed_time' do
      subject.elapsed_time(dep.to_s, arr.to_s).must_equal '2h13'
      subject.elapsed_time(dep.to_s,
                           (arr - 5.minutes).to_s).must_equal '2h08'
    end
  end
end
