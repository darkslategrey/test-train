require 'spec_helper'

describe Trainline::Spider do

  subject { Trainline::Spider.new({browser: browser}) }

  before {
    file = "#{File.dirname(__FILE__)}/../../config/train.yml"
    Trainline::Conf.instance.load(file)
  }
  let(:json_response) {
    "#{File.dirname(__FILE__)}/../fixtures/response.json"
  }
  let(:gzipped_response) {
    out      = StringIO.new
    zipped   = Zlib::GzipWriter.new(out)
    zipped.write(IO.read(json_response)); zipped.close
    out.string
  }
  let(:browser) {
    Faraday.new do |builder|
      builder.adapter :test do |stub|
        stub.post('/api/v5_1/search') { |env| [200, {}, gzipped_response] }
      end
    end
  }

  let(:search_params) {
    json_file = "#{File.dirname(__FILE__)}/../fixtures/search_params.json"
    JSON.parse(IO.read(json_file), symbolize_names: true)
  }

  it 'can_be_instanciated' do
    subject.wont_be_nil
  end

  it 'should_query_search_api' do
    response = subject.search(search_params)
    response[:folders].wont_be_nil
  end
end
