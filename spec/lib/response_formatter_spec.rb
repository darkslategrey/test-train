require 'spec_helper'

describe Trainline::ResponseFormatter do

  subject { Trainline::ResponseFormatter.new(response) }

  let(:response) {
    file = "#{File.dirname(__FILE__)}/../fixtures/response.json"
    JSON.parse(IO.read(file), symbolize_names: true)
  }

  it 'can_be_instantiated' do
    subject.wont_be_nil
  end

  describe '#format' do
    it 'format_response' do
      subject.format.to_s.must_match /156,00 Euros/
    end
  end

  describe '#select_lower_prices' do
    let(:travels) { subject.extract_main_travels_attrs }

    it 'extract_lower_prices' do
      subject.select_lower_prices(travels).size.must_equal 11
    end
  end

  describe '#extract_main_travels_attrs' do
    let(:travels_data) {
      file = "#{File.dirname(__FILE__)}/../fixtures/response.json"
      JSON.parse(IO.read(file))
    }
    let(:attendee) {
      {
        arrival_date: '2017-11-10T11:40:00+01:00',
        departure_date: '2017-11-10T01:35:00+01:00',
        system: 'flixbus',
        travel_class: 'economy',
        cents: 3990
      }
    }
    it 'should_extract_main_data' do
      extracted = subject.extract_main_travels_attrs
      extracted.first.must_equal attendee
    end
  end
end
