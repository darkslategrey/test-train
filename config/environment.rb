$:.unshift File.dirname(__FILE__) + "/../lib"

require 'trainline'
require 'active_record'
ActiveRecord::Base.establish_connection(adapter: 'sqlite3',
                                        database: 'db/dev.sqlite3')
