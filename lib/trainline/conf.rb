# frozen_string_literal: true

require 'singleton'
require 'yaml'

module Trainline
  # Hold the configuration for all the app
  class Conf
    include Singleton

    attr_accessor :host, :search_path, :headers, :default_search_params

    def load(file)
      conf = YAML.load_file(file)
      @host        = conf.fetch(:host)
      @search_path = conf.fetch(:search_path)
      @headers     = conf.fetch(:headers)
      @default_search_params = conf.fetch(:default_search_params)
      self
    end
  end
end
