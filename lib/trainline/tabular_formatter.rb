# frozen_string_literal: true

require 'tabularize'

module Trainline
  # Convert travels hash to tabular string
  # +--------|---------|-------------|-----------|-------|---------+
  # | depart | arrivee | prix        | transport | duree | confort |
  #   +--------|---------|-------------|-----------|-------|---------+
  # | 17:37  | 20:54   | 72,00 Euros | sncf      | 3h17  | economy |
  # | 17:37  | 20:54   | 82,00 Euros | sncf      | 3h17  | first   |
  # | 18:19  | 21:46   | 53,00 Euros | sncf      | 3h27  | economy |
  class TabularFormatter
    def initialize(travels)
      @travels = travels
    end

    def format
      table = Tabularize.new
      table << table_headers
      table.separator!

      @travels.each do |travel|
        table << format_line(travel)
      end
      table
    end

    def format_line(travel)
      departure = Utils.to_time(travel[:departure_date])
      arrival   = Utils.to_time(travel[:arrival_date])
      price     = Utils.format_price(travel[:cents])
      elapsed   = Utils.elapsed_time(travel[:departure_date],
                                     travel[:arrival_date])
      [departure, arrival, price, travel[:system], elapsed,
       travel[:travel_class]]
    end

    def table_headers
      %w[depart arrivee prix transport duree confort]
    end
  end
end
