# frozen_string_literal: true

module Trainline
  # This provide what will be show to the end user as result of his query.
  #
  class ResponseFormatter
    def initialize(response, formatter = TabularFormatter)
      @response  = response
      @formatter = formatter
    end

    def format
      travels = extract_main_travels_attrs
      travels = select_lower_prices(travels)
      @formatter.new(travels).format
    end

    def extract_main_travels_attrs
      main_attrs = []
      @response[:folders].map do |folder|
        attrs = %w[arrival_date departure_date system travel_class cents]
        attrs = attrs.map do |attr|
          [attr.to_sym, folder[attr.to_sym]]
        end
        main_attrs << Hash[attrs]
      end
      main_attrs
    end

    def select_lower_prices(travels)
      lower_price_travels = []
      lowest_prices       = {}

      travels.each do |travel|
        (lower_price_travels << travel) if lowest_price?(travel,
                                                         lowest_prices)
      end
      lower_price_travels
    end

    def lowest_price?(travel, prices)
      key = build_key(travel)
      prices[key] ||= 999_999
      if travel[:cents] < prices[key]
        prices[key] = travel[:cents]
        return true
      end
      false
    end

    def build_key(travel)
      key = "#{travel[:system]}#{travel[:departure_date]}"
      "#{key}#{travel[:arrival_date]}#{travel[:travel_class]}"
    end
  end
end
