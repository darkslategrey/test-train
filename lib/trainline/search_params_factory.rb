# frozen_string_literal: true

module Trainline
  # Fill mandatory params to query API
  class SearchParamsFactory
    attr_reader :params

    def initialize(from, to, date)
      @params = Conf.instance.default_search_params
      @params[:search][:departure_date]       = Utils.to_date(date)
      @params[:search][:departure_station_id] = City.find_by_name(from).code
      @params[:search][:arrival_station_id]   = City.find_by_name(to).code
    end
  end
end
