# frozen_string_literal: true

require 'faraday'
require 'faraday-cookie_jar'

module Trainline
  # Do the query to the API
  class Spider
    def initialize(params = {})
      @host        = Conf.instance.host
      @search_path = Conf.instance.search_path
      @headers     = Conf.instance.headers
      @browser     = params.fetch(:browser, default_browser)
    end

    def search(params = {})
      @browser.headers.merge!(@headers)
      @browser.send(:post, @search_path, params)
      response = @browser.send(:post, @search_path, params)
      Utils.gunzip_to_hash(response.body)
    end

    def default_browser
      Faraday.new(url: @host) do |faraday|
        faraday.request  :url_encoded
        faraday.request  :multipart
        # faraday.response :logger
        faraday.use :cookie_jar
        faraday.adapter :excon
      end
    end
  end
end
