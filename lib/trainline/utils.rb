# frozen_string_literal: true

require 'zlib'
require 'time_difference'

module Trainline
  # Utility class for misc tools
  class Utils
    class << self
      def gunzip_to_hash(zipped)
        unzipped = Zlib::GzipReader.new(StringIO.new(zipped)).read
        JSON.parse(unzipped, symbolize_names: true)
      end

      # DD/MM/YY HH:MM => 2017-11-10T06:00:00UTC
      def to_date(date)
        Time.strptime(date, '%d/%m/%y %H:%M').to_datetime.to_s
      end

      # 2017-11-10T06:00:00UTC => 06:00
      def to_time(date)
        Time.parse(date).strftime('%H:%M')
      end

      # 1234 = 12,34 Euros
      def format_price(price)
        format('%.2f Euros', price.to_f / 100).sub('.', ',')
      end

      # 3h04
      def elapsed_time(dep, arr)
        dep  = Time.parse(dep)
        arr  = Time.parse(arr)
        time = TimeDifference.between(dep, arr).humanize
        time.sub!(' Hours and ', 'h')
        time.sub(/h([1-9]) Minutes/, 'h0\1').sub(' Minutes', '')
      end
    end
  end
end
