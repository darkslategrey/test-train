# frozen_string_literal: true

require 'pry'
require 'json'
require 'active_record'
require 'pp'

require_relative 'trainline/conf'
require_relative 'trainline/utils'
require_relative 'trainline/spider'
require_relative 'trainline/search_params_factory'
require_relative 'trainline/city'
require_relative 'trainline/response_formatter'
require_relative 'trainline/tabular_formatter'
module Trainline
  # This is the entry point for the app
  class Main
    # @param params [Hash] with keys:
    # `from`: Departure city
    # `to`:   Arrival city
    # `date`: Travel Date 'DD/MM/YY HH:MM'
    def initialize(params)
      @from = params.fetch('from')
      @to   = params.fetch('to')
      @date = params.fetch('date')
      @conf = Conf.instance.load(params.fetch('conf'))
    end

    # Run the travel search on Trainline API
    def search
      search_params = SearchParamsFactory.new(@from, @to, @date).params
      response      = Spider.new.search(search_params)
      puts "\n\nDE '#{@from}' A '#{@to}' LE #{@date}\n\n"
      puts ResponseFormatter.new(response).format
    end
  end
end
