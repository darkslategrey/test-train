#!/usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH.unshift File.dirname(__FILE__) + '/../lib'

require 'trainline'
require_relative '../config/environment'

require 'thor'

module Trainline
  # CLI entry point
  class CLI < Thor
    class_option :conf,
                 default: './config/train.yml',
                 desc: 'alternate config file'

    desc 'search', 'Search for available travels between two cities'
    method_option :from, type: :string, aliases: '-f',
                         required: true, desc: 'Departure city',
                         default: 'Paris', banner: '"CITY"'
    method_option :to, type: :string, aliases: '-t',
                       required: true, desc: 'Arrival city',
                       default: 'Marseille Saint Charles',
                       banner: '"ANOTHER_CITY"'
    method_option :date, type: :string, aliases: '-d',
                         required: true, desc: 'Departure Date',
                         banner: '"DD/MM/YY HH:MM"',
                         default: Time.now.next_day.strftime('%d/%m/%y %H:%M')
    def search
      Trainline::Main.new(options).search
    end

    desc 'list', 'List available cities'
    def list
      puts 'Available cities :'
      City.all.each do |city|
        puts "\t* #{city.name}"
      end
    end
  end
end

Trainline::CLI.start(ARGV)
