# Trainline CLI

Cette petite application permet de lister les trajets entre deux villes pour un
jour et une heure donnée.

C'est l'API Trainline `/api/v5_1/search` qui est interrogée.

## Installation

Ruby version : 2.3

```shell
$ git clone https://gitlab.com/darkslategrey/test-train.git
$ cd test-train
$ bundle
```

## Usage

Par défaut la recherche se fait pour les trajets de `Paris` à `Marseille Saint
Charles` le jour suivant : 

```shell
$ date '+%d/%m/%y %H:%M'
10/11/17 19:22

$ ./bin/train-cli search


DE 'Paris' A 'Marseille Saint Charles' LE 11/11/17 19:22

+--------|---------|-------------|-----------|-------|---------+
| depart | arrivee | prix        | transport | duree | confort |
+--------|---------|-------------|-----------|-------|---------+
| 17:37  | 20:54   | 72,00 Euros | sncf      | 3h17  | economy |
| 17:37  | 20:54   | 82,00 Euros | sncf      | 3h17  | first   |
| 18:19  | 21:46   | 53,00 Euros | sncf      | 3h27  | economy |
| 18:19  | 21:46   | 68,90 Euros | sncf      | 3h27  | first   |
| 18:37  | 21:57   | 72,00 Euros | sncf      | 3h20  | economy |
| 18:37  | 21:57   | 82,00 Euros | sncf      | 3h20  | first   |
| 19:37  | 22:54   | 76,00 Euros | sncf      | 3h17  | economy |
| 19:37  | 22:54   | 87,00 Euros | sncf      | 3h17  | first   |
| 20:00  | 05:55   | 35,90 Euros | flixbus   | 9h55  | economy |
| 20:10  | 23:58   | 29,90 Euros | idtgv     | 3h48  | first   |
| 20:10  | 23:58   | 24,00 Euros | idtgv     | 3h48  | economy |
| 20:10  | 23:58   | 42,00 Euros | sncf      | 3h48  | economy |
| 20:10  | 23:58   | 55,00 Euros | sncf      | 3h48  | first   |
+--------|---------|-------------|-----------|-------|---------+
```

On peut préciser une date via l'option `-d` :

```shell
$ ./bin/train-cli -d '11/11/17 19:22'
```

Une aide succincte est disponible. Par exemple pour la commande `search`

```shell
$ ./bin/train-cli help search
Usage:
  train-cli search -d, --date="DD/MM/YY HH:MM" -f, --from="CITY" -t, --to="ANOTHER_CITY"

Options:
  -f, --from="CITY"            # Departure city
                               # Default: Paris
  -t, --to="ANOTHER_CITY"      # Arrival city
                               # Default: Marseille Saint Charles
  -d, --date="DD/MM/YY HH:MM"  # Departure Date
                               # Default: 11/11/17 19:25
      [--conf=CONF]            # alternate config file
                               # Default: ./config/train.yml

Search for available travels between two cities
```

Pour lister les villes disponibles :

```shell
$ ./bin/train-cli list
Available cities :
    * Paris
    * Marseille Saint Charles
[greg@citron cli]$
```

## Configuration

Le fichier de config par défaut est `config/train.yml`. Il contient les éléments
pour requêter l'api Trainline.

```yaml
---
:host: 'https://www.trainline.fr'
:search_path: '/api/v5_1/search'
:headers:
  :user_agent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'
  :accept-encoding: 'gzip, deflate, br'
  :x-user-agent: 'CaptainTrain/1510236308(web) (Ember 2.12.2)'

:default_search_params:
  :search:
    :departure_date:
    :return_date:
    :passengers:
    - :id: 50017e37-8372-4128-b0d2-02fc0431a00b
      :label: adult
      :age: 26
      :cards: []
      :cui:
    :systems:
    - sncf
    - db
    - idtgv
    - ouigo
    - trenitalia
    - ntv
    - hkx
    - renfe
    - benerail
    - ocebo
    - westbahn
    - locomore
    - busbud
    - flixbus
    - distribusion
    - timetable
    :exchangeable_part:
    :source: ec18c9
    :departure_station_id:
    :via_station_id:
    :arrival_station_id:
    :exchangeable_pnr_id:
```

## Limitations

Ceci est un exercice c'est pourquoi les fonctionnalités proposées sont très
limitées :

- Deux villes disponibles;
- Pas de carte de réductions;
- Un seul voyageur de +26 ans;
- Trajets sur un seul sens pas de retour;
- ...

## Tests

```shell
rake
```

## TODOS

Beaucoup de choses peuvent être ajoutées. Parmi celles-ci :

- Utiliser une interface plus interactive
  [highline](https://github.com/JEG2/highline);
- Ajouter d'autres villes;
- Permettre de lister les trajets suivants / précédents comme l'app web;
- C'est un premier jet de code, il y a encore quelques frictions dans le code;
- Stocker en base les données collectées (suivi des tarifs);
- J'ai fait l'impasse sur les exceptions/loggers;
- ...


## Démarche

Je suis un grand fan de la `CLI`, du `scraping` et de la manipulation des
données c'est pourquoi j'ai choisi ce type d'app.

### Quels échanges avec le serveur ?

Mon premier objectif à été de déterminer la nature et le contenu des échanges
entre le client web et le serveur.

Avec l'aide des outils de devs disponibles dans le navigateur (l'onglet
`network` principalement), j'ai étudié les messages qu’échangeaient l'app et le
serveur à chacune de mes actions sur l'interface.

### Identifier la requête pertinente

Pour trouvé la requête qui renvoi les données des trajets, le navigateur
`Chrome` permet de sauvegarder la requête de son choix au format `curl`.

J'ai donc pu reproduire hors du navigateur certaines requêtes et vérifier les
données retournées.


```shell
curl 'https://www.trainline.fr/api/v5_1/search'
-H 'cookie: _ct_ai_referral=%257B%2522source%2522%253A%2522ec18c9%2522%257D; _ga=GA1.2.1627783306.1510230700; _gid=GA1.2.347324416.1510230700; _gac_UA-16633907-29=1.1510230700.EAIaIQobChMIg6H72L-x1wIVaLXtCh1ZUA4vEAAYASAAEgJxXvD_BwE; _uetsid=_uete82ebc18; _gat_ctTracker=1'
-H 'x-ct-client-id: 2ae9e9a7-3c95-4739-b021-31951f69fca6'
-H 'origin: https://www.trainline.fr'
-H 'accept-encoding: gzip, deflate, br'
-H 'x-ct-timestamp: 1510236308'
-H 'accept-language: fr-FR,fr;q=0.8'
-H 'x-ct-locale: fr'
-H 'x-requested-with: XMLHttpRequest'
-H 'pragma: no-cache'
-H 'x-user-agent: CaptainTrain/1510236308(web) (Ember 2.12.2)'
-H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'
-H 'content-type: application/json; charset=UTF-8'
-H 'accept: application/json, text/javascript, */*; q=0.01'
-H 'cache-control: no-cache'
-H 'authority: www.trainline.fr'
-H 'referer: https://www.trainline.fr/search'
-H 'x-ct-version: 8206723ea56ed1d0e0b7b239c7a7671dd8ada352'

--data-binary '{"search":{"departure_date":"2017-11-10T16:00:00UTC","return_date":"2017-11-11T16:00:00UTC","passengers":[{"id":"50017e37-8372-4128-b0d2-02fc0431a00b","label":"adult","age":26,"cards":[],"cui":null}],"systems":["sncf","db","idtgv","ouigo","trenitalia","ntv","hkx","renfe","benerail","ocebo","westbahn","locomore","busbud","flixbus","distribusion","timetable"],"exchangeable_part":null,"source":"ec18c9","departure_station_id":"4924","via_station_id":null,"arrival_station_id":"4791","exchangeable_pnr_id":null}}
```

### Analyser les données retournées

Une fois les données récupérées (en JSON) j'ai utilisé l'outil
[jq](https://github.com/stedolan/jq) qui m'a permis de naviguer facilement dans
la structure pour y chercher les trajets.

```shell
$ jq '.|keys'  spec/fixtures/response.json
[
  "comfort_classes",
  "conditions",
  "folders",
  "passengers",
  "search",
  "segments",
  "stations",
  "trips"
]
```

J'ai opté pour ce que contenait la clé `folders`.

### Difficultés

#### Automatiser les interactions

J'ai quelque peu tâtonné pour trouver la bonne approche sur la manière
d'automatiser mes interaction avec le serveur.

Une première approche à été de me servir de `selenium`, `capybara` avec `chrome
headless`. Le front étant codé en `Ember`, il me fallait un moteur js.
Après quelques tentatives et aidé de screenshot intermédiaire, j'ai renoncé à
cette technique. Trop gourmand en ressources et trop long.
Mais c'est surtout l'impossibilité de positionner de dates de départ et d'arrivée
qui m'ont décidé à chercher une autre solution et à me tourner vers un autre
client http : [faraday](https://github.com/lostisland/faraday) avec
[excon](https://github.com/excon/excon#readme).

#### Requêter l'API

J'ai pris aussi un peu de temps afin d'identifier les headers http indispensable
au requêtage de l'API.
Ainsi j'ai pu trouvé que seul le `x-user-agent` était nécessaire.

#### Exploiter les données retournées

Les données retournées par l'API étaient bien plus nombreuses que celles
affichées via le browser. Après analyse, j'ai compris que les trajets en bus
étaient aussi retournés.

Également, pour un même horaire et un même moyen de transport, plusieurs tarifs
étaient retournés. J'ai fais le choix de ne retenir que le prix minimum. Il me
semble que c'est de cette manière que l'app `Ember` procède.


#### Ajouter des gares

Bien sûr il m'a manqué les codes des gares pour les paramètres
`arrival_station_id` et `departure_station_id`
J'ai choisi les deux premières villes de France pour cet exercice.

#### Consulter les tarifs suivants et/ou précédents

C'est possible sur l'app et j'ai d'abord envisagé de l'implémenter dans la CLI.
Mais par manque de temps j'ai renoncé.

Cela dit, j'ai mis un peu de temps à décortiquer l'url de l'API pour cette
fonctionnalité et j'ai finis par dégager ce format :

```shell
curl
'https://www.trainline.fr/api/v5_1/search/SEARCH_ID/FOLDER_ID?comfort_classes%5B%5D=SEGMENT_ID%3Asncf.regular'
```

Afin de construire cette url à partir du premier jeu de données il faut donc
récupérer les éléments suivants :

- `SEARCH_ID`;
- `FOLDER_ID` (sans être sûr : l'id du dernier trajet affiché ?);
- `SEGMENT_ID` (Via les `trips` du json qui font le lien entre un folder et un
  segment ?)
 
  ```shell
  $ jq '.folders[0]|keys'  spec/fixtures/response.json
  [
  "affiliation_label",
  # ...
  "id",                    # FOLDER ID
  "search_id",             # LE SEARCH
  "system",
  "travel_class",
  "trip_ids"               # LE TRIP
  ]

  $ jq '.trips[0]|keys'  spec/fixtures/response.json
  [
  "arrival_date",
  "arrival_station_id",
  "cents",
  "currency",
  "departure_date",
  "departure_station_id",
  "digest",
  "folder_id",                # LE FOLDER
  "id",
  "local_amount",
  "local_currency",
  "passenger_id",
  "segment_ids"               # LE SEGMENT
  ]

  $ jq '.trips[0].segment_ids|keys'  spec/fixtures/response.json
  [
    0
  ]
```
